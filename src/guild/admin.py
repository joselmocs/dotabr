from django.contrib import admin

from guild.models import Player


class PlayerAdmin(admin.ModelAdmin):
    list_display = ('name',)
    ordering = ('name',)
    search_fields = ('name',)


admin.site.register(Player, PlayerAdmin)
