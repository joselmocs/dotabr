from django.contrib import admin

from wow.models import Raid, Boss, Item, LootTable


class RaidAdmin(admin.ModelAdmin):
    list_display = ('name', 'rid',)
    ordering = ('id',)


class LootTableInlineAdmin(admin.TabularInline):
    model = LootTable
    autocomplete_fields = ('item',)


class BossAdmin(admin.ModelAdmin):
    list_display = ('name', 'raid', 'rid')
    ordering = ('raid', 'id',)
    list_filter = ('raid',)
    inlines = (LootTableInlineAdmin,)
    search_fields = ('name',)


class ItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'rid',)
    ordering = ('id',)
    search_fields = ('name',)


class BossListFilter(admin.SimpleListFilter):
    title = 'Boss'
    parameter_name = 'boss__id__exact'

    def lookups(self, request, model_admin):
        raid_id = request.GET.get('boss__raid__id__exact')

        response = []
        if not raid_id:
            return response

        bosses = Boss.objects.filter(raid__id=raid_id)
        for boss in bosses:
            response.append((boss.id, boss.name))

        return response

    def queryset(self, request, queryset):
        boss_id = request.GET.get('boss__id__exact')
        if not boss_id:
            return queryset

        return queryset.filter(boss=boss_id)


class LootTableAdmin(admin.ModelAdmin):
    list_display = ('item', 'boss',)
    ordering = ('boss',)
    list_filter = ('boss__raid', BossListFilter,)
    autocomplete_fields = ('item', 'boss',)
    search_fields = ('item__name',)


admin.site.register(Raid, RaidAdmin)
admin.site.register(Boss, BossAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(LootTable, LootTableAdmin)
