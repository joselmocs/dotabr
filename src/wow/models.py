from django.db import models


class Raid(models.Model):
    rid = models.IntegerField()
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name


class Boss(models.Model):
    class Meta:
        verbose_name_plural = "Bosses"

    rid = models.IntegerField()
    name = models.CharField(max_length=64)
    raid = models.ForeignKey("wow.Raid", on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class Item(models.Model):
    rid = models.IntegerField()
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name


class LootTable(models.Model):
    class Meta:
        unique_together = [['item', 'boss']]

    item = models.ForeignKey("wow.Item", on_delete=models.PROTECT)
    boss = models.ForeignKey("wow.Boss", on_delete=models.PROTECT)

    def __str__(self):
        return "{}/{}".format(self.boss, self.item)
