migrate:
	python src/manage.py makemigrations
	python src/manage.py migrate

run:
	python src/manage.py runserver_plus

build:
	python src/manage.py collectstatic --noinput

shell:
	python src/manage.py shell_plus
